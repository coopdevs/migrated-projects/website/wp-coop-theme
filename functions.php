<?php

/**
 * Theme setup.
 */
function wpct_setup() {
  // Make theme available for translation.
  load_theme_textdomain( 'wpct', get_template_directory() . '/languages' );
  // THEME SUPPORT
  add_theme_support( 'title-tag' );
  add_theme_support(
    'html5',
    array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    )
  );
  add_theme_support( 'menus' );
  add_theme_support( 'custom-logo' );
  add_theme_support( 'post-thumbnails' );
  add_theme_support( 'custom-line-height' );
  add_theme_support( 'align-wide' );
  // Add support for block styles.
  add_theme_support( 'wp-block-styles' );
  // Enqueue editor styles.
  add_theme_support( 'editor-styles' );
  add_editor_style( 'css/app.css' );
}
add_action( 'after_setup_theme', 'wpct_setup' );

/**
 * Enqueue theme assets.
 */
function wp_coop_theme_enqueue_scripts() {
  $theme = wp_get_theme();
  wp_enqueue_style( 'wp-coop-theme', wp_coop_theme_asset( 'css/app.css' ), array(), $theme->get( 'Version' ) );
  wp_enqueue_script( 'wp-coop-theme', wp_coop_theme_asset( 'js/app.js' ), array(), $theme->get( 'Version' ) );
}
add_action( 'wp_enqueue_scripts', 'wp_coop_theme_enqueue_scripts' );

/**
 * Get asset path.
 *
 * @param string  $path Path to asset.
 *
 * @return string
 */
function wp_coop_theme_asset( $path ) {
  if ( wp_get_environment_type() === 'production' ) {
    return get_template_directory_uri() . '/' . $path;
  }
  return add_query_arg( 'time', time(),  get_template_directory_uri() . '/' . $path );
}

/**
 * Enqueue fonts.
 */
function wpct_enqueue_fonts() {
  wp_enqueue_style( 'wpct-fonts', wpct_fonts_url(), array(), null );
}
add_action( 'wp_enqueue_scripts', 'wpct_enqueue_fonts' );

// Define fonts.
function wpct_fonts_url() {
  // Allow child themes to disable to the default Coop Theme fonts.
  $dequeue_fonts = apply_filters( 'wpct_dequeue_fonts', false );
  if ( $dequeue_fonts ) {
    return '';
  }
  $fonts = array(
    'family=Outfit:wght@100;200;300;400;500;600;700;800;900',
  );

  // Make a single request for all Google Fonts.
  return esc_url_raw( 'https://fonts.googleapis.com/css2?' . implode( '&', array_unique( $fonts ) ) . '&display=swap' );

}

/**
 * Register block styles.
 *
 */
function wpct_register_block_styles() {

  $block_styles = array(
    'core/button'          => array(
      'fill-base'    => __( 'Fill Base', 'wpct' ),
      'outline-base' => __( 'Outline Base', 'wpct' ),
    ),
    'core/group'           => array(
      'boxshadow'       => __( 'Boxshadow', 'wpct' ),
      'boxshadow-solid' => __( 'Boxshadow Solid', 'wpct' ),
      'full-height'     => __( 'Full-height', 'wpct' ),
    ),
    'core/image'           => array(
      'boxshadow' => __( 'Boxshadow', 'wpct' ),
    ),
    'core/list'           => array(
      'no-disc' => __( 'No Disc', 'wpct' ),
    ),
    'core/media-text'      => array(
      'boxshadow-media' => __( 'Boxshadow', 'wpct' ),
    ),
    'core/navigation-link' => array(
      'fill'         => __( 'Fill', 'wpct' ),
      'fill-base'    => __( 'Fill Base', 'wpct' ),
      'outline'      => __( 'Outline', 'wpct' ),
      'outline-base' => __( 'Outline Base', 'wpct' ),
    ),
  );

  foreach ( $block_styles as $block => $styles ) {
    foreach ( $styles as $style_name => $style_label ) {
      register_block_style(
        $block,
        array(
          'name'  => $style_name,
          'label' => $style_label,
        )
      );
    }
  }
}
add_action( 'init', 'wpct_register_block_styles' );

/**
 * Registers block categories, and type.
 *
 */
function wpct_register_block_pattern_categories() {

  /* Functionality specific to the Block Pattern Explorer plugin. */
  if ( function_exists( 'register_block_pattern_category_type' ) ) {
    register_block_pattern_category_type( 'wpct', array( 'label' => __( 'WP Coop Theme', 'wpct' ) ) );
  }

  $block_pattern_categories = array(
    'wpct-footer'  => array(
      'label'         => __( 'Footer', 'wpct' ),
      'categoryTypes' => array( 'wpct' ),
    ),
    'wpct-general' => array(
      'label'         => __( 'General', 'wpct' ),
      'categoryTypes' => array( 'wpct' ),
    ),
    'wpct-header'  => array(
      'label'         => __( 'Header', 'wpct' ),
      'categoryTypes' => array( 'wpct' ),
    ),
    'wpct-page'    => array(
      'label'         => __( 'Page', 'wpct' ),
      'categoryTypes' => array( 'wpct' ),
    ),
    'wpct-query'   => array(
      'label'         => __( 'Query', 'wpct' ),
      'categoryTypes' => array( 'wpct' ),
    ),
  );

  foreach ( $block_pattern_categories as $name => $properties ) {
    register_block_pattern_category( $name, $properties );
  }
}
add_action( 'init', 'wpct_register_block_pattern_categories', 9 );

/**
 * Reusable Blocks accessible in backend
 *
 */
function wpct_reusable_blocks_admin_menu() {
    add_menu_page( 'Reusable Blocks', 'Reusable Blocks', 'edit_posts', 'edit.php?post_type=wp_block', '', 'dashicons-editor-table', 22 );
}
add_action( 'admin_menu', 'wpct_reusable_blocks_admin_menu' );
