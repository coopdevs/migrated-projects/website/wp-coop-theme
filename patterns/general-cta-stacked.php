<?php
/**
 * Title: Section: Call-to-action-stacked (text, button).
 * Slug: wpct/general-cta-stacked
 * Categories: wpct-general
 */

?>
<!-- wp:group {"align":"full","style":{"spacing":{"margin":{"top":"0px"}}},"layout":{"inherit":true}} -->
<div class="wp-block-group alignfull" style="margin-top:0px">
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
<!-- wp:heading {"textAlign":"center","style":{"spacing":{"margin":{"bottom":"var(--wp--custom--spacing--sxs)"}},"typography":{"fontStyle":"normal","fontWeight":"var(--wp--custom--font-weight--regular)"}},"fontSize":"x-large-sxl"} -->
<h2 class="has-text-align-center has-x-large-sxl-font-size" id="let-s-connect" style="font-style:normal;font-weight:var(--wp--custom--font-weight--regular);margin-bottom:var(--wp--custom--spacing--sxs)"><?php echo esc_html__( 'Let’s Connect', 'wpct' ); ?></h2>
<!-- /wp:heading -->
<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">Quisque aliquam nisl quis metus taylor feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing vestibulum vitae gravida non diam accumsan.</p>
<!-- /wp:paragraph -->
<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons">
<!-- wp:button {"className":"is-style-outline"} -->
<div class="wp-block-button is-style-outline"><a class="wp-block-button__link"><?php echo esc_html__( 'Get in Touch', 'wpct' ); ?> →</a></div>
<!-- /wp:button -->
</div>
<!-- /wp:buttons -->
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
</div>
<!-- /wp:group -->
