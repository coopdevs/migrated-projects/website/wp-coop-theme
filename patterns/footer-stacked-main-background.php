<?php
/**
 * Title: Footer: Stacked background-color-main (heading, text, button).
 * Slug: wpct/footer-stacked-main-background
 * Categories: wpct-footer
 */

?>
<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|main-contrast"}}},"spacing":{"padding":{"top":"var(--wp--custom--spacing--sspacer)","bottom":"var(--wp--custom--spacing--sxl)"},"margin":{"top":"0px"}}},"backgroundColor":"main","textColor":"main-contrast","layout":{"inherit":true}} -->
<div class="wp-block-group alignfull has-main-contrast-color has-main-background-color has-text-color has-background has-link-color" style="margin-top:0px;padding-top:var(--wp--custom--spacing--sspacer);padding-bottom:var(--wp--custom--spacing--sxl)">
<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"var(--wp--custom--font-weight--regular)"},"spacing":{"margin":{"bottom":"var(--wp--custom--spacing--sxs)"}}},"fontSize":"x-large-sxl"} -->
<h2 class="has-text-align-center has-x-large-sxl-font-size" id="let-s-connect" style="font-style:normal;font-weight:var(--wp--custom--font-weight--regular);margin-bottom:var(--wp--custom--spacing--sxs)"><?php echo esc_html__( 'Let’s Connect', 'wpct' ); ?></h2>
<!-- /wp:heading -->
<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">Quisque aliquam nisl quis metus taylor feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing vestibulum vitae gravida non diam accumsan.</p>
<!-- /wp:paragraph -->
<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center","orientation":"horizontal"}} -->
<div class="wp-block-buttons">
<!-- wp:button {"style":{"border":{"radius":0}},"className":"is-style-outline-main-contrast"} -->
<div class="wp-block-button is-style-outline-main-contrast"><a class="wp-block-button__link no-border-radius" href="#"><?php echo esc_html__( 'Get in Touch', 'wpct' ); ?> →</a></div>
<!-- /wp:button -->
</div>
<!-- /wp:buttons -->
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
<!-- wp:paragraph {"align":"center","fontSize":"small"} -->
<p class="has-text-align-center has-small-font-size">© <?php echo gmdate( 'Y' ); ?> Your Cooperative <a href="#"><?php echo esc_html__( 'Contact Us', 'wpct' ); ?></a></p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:group -->
