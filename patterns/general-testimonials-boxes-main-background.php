<?php
/**
 * Title: Section: Testimonials-boxes background-color-main (text).
 * Slug: wpct/general-testimonials-boxes-main-background
 * Categories: wpct-general
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"spacing":{"margin":{"top":"0px"}}},"backgroundColor":"main","layout":{"inherit":true}} -->
<div class="wp-block-group alignfull has-main-background-color has-background" style="margin-top:0px">
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide">
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:group {"style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxl)","right":"var(--wp--custom--spacing--sxl)","bottom":"var(--wp--custom--spacing--sxl)","left":"var(--wp--custom--spacing--sxl)"}}},"backgroundColor":"base"} -->
<div class="wp-block-group has-base-background-color has-background" style="padding-top:var(--wp--custom--spacing--sxl);padding-right:var(--wp--custom--spacing--sxl);padding-bottom:var(--wp--custom--spacing--sxl);padding-left:var(--wp--custom--spacing--sxl)">
<!-- wp:heading {"textAlign":"center","level":4,"style":{"typography":{"fontSize":72,"lineHeight":"1"}}} -->
<h4 class="has-text-align-center" style="font-size:72px;line-height:1">“</h4>
<!-- /wp:heading -->
<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center" style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum sit amet, consectetur a adipiscing. Nulla vitae lorem a neque imperdiet sagittis vivamus enim velit.</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph {"align":"center","fontSize":"small"} -->
<p class="has-text-align-center has-small-font-size"><strong>—Allison Taylor, Designer</strong></p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:group -->
</div>
<!-- /wp:column -->
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:group {"style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxl)","right":"var(--wp--custom--spacing--sxl)","bottom":"var(--wp--custom--spacing--sxl)","left":"var(--wp--custom--spacing--sxl)"}}},"backgroundColor":"base"} -->
<div class="wp-block-group has-base-background-color has-background" style="padding-top:var(--wp--custom--spacing--sxl);padding-right:var(--wp--custom--spacing--sxl);padding-bottom:var(--wp--custom--spacing--sxl);padding-left:var(--wp--custom--spacing--sxl)">
<!-- wp:heading {"textAlign":"center","level":4,"style":{"typography":{"fontSize":72,"lineHeight":"1"}}} -->
<h4 class="has-text-align-center" style="font-size:72px;line-height:1">“</h4>
<!-- /wp:heading -->
<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center" style="font-size:var(--wp--preset--font-size--small)">Fusce at est sapien. Aliquam tempus nulla nisipt rhoncus, morbi et convallis magna rhoncus morbi viverra ante.</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph {"align":"center","fontSize":"small"} -->
<p class="has-text-align-center has-small-font-size"><strong>—Anthony Breck, Developer</strong></p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:group -->
</div>
<!-- /wp:column -->
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:group {"style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxl)","right":"var(--wp--custom--spacing--sxl)","bottom":"var(--wp--custom--spacing--sxl)","left":"var(--wp--custom--spacing--sxl)"}}},"backgroundColor":"base"} -->
<div class="wp-block-group has-base-background-color has-background" style="padding-top:var(--wp--custom--spacing--sxl);padding-right:var(--wp--custom--spacing--sxl);padding-bottom:var(--wp--custom--spacing--sxl);padding-left:var(--wp--custom--spacing--sxl)">
<!-- wp:heading {"textAlign":"center","level":4,"style":{"typography":{"fontSize":72,"lineHeight":"1"}}} -->
<h4 class="has-text-align-center" style="font-size:72px;line-height:1">“</h4>
<!-- /wp:heading -->
<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center" style="font-size:var(--wp--preset--font-size--small)">Quisque ullamcorp nulla elementum, atipo consectetur iaculis vestibulum et faucibus vitae milano pellentesque.</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph {"align":"center","fontSize":"small"} -->
<p class="has-text-align-center has-small-font-size"><strong>—Rebecca Jones, Coach</strong></p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:group -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
</div>
<!-- /wp:group -->
