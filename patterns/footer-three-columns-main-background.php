<?php
/**
 * Title: Footer: 3-columns background-color-main (text, links, buttons).
 * Slug: wpct/footer-three-columns-main-background
 * Categories: wpct-footer
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}},"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxl)","bottom":"var(--wp--custom--spacing--sxl)"},"margin":{"top":"0px"}}},"backgroundColor":"main","textColor":"base","layout":{"inherit":true},"fontSize":"small"} -->
<div class="wp-block-group alignfull has-base-color has-main-background-color has-text-color has-background has-link-color has-small-font-size" style="margin-top:0px;padding-top:var(--wp--custom--spacing--sxl);padding-bottom:var(--wp--custom--spacing--sxl)">
<!-- wp:group {"align":"wide","layout":{"type":"flex","allowOrientation":false,"justifyContent":"space-between"}} -->
<div class="wp-block-group alignwide">
<!-- wp:paragraph -->
<p>© <?php echo gmdate( 'Y' ); ?> Your Cooperative</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph -->
<p><a href="#"><?php echo esc_html__( 'Privacy Policy', 'wpct' ); ?></a> · <a href="#"><?php echo esc_html__( 'Terms of Service', 'wpct' ); ?></a> · <a href="#"><?php echo esc_html__( 'Contact Us', 'wpct' ); ?></a></p>
<!-- /wp:paragraph -->
<!-- wp:social-links {"iconColor":"main","iconColorValue":"var(--wp--preset--color--main)","iconBackgroundColor":"base","iconBackgroundColorValue":"var(--wp--preset--color--base)","className":"is-style-default","style":{"spacing":{"blockGap":"var(--wp--custom--spacing--sxxs)"}}} -->
<ul class="wp-block-social-links has-icon-color has-icon-background-color is-style-default">
<!-- wp:social-link {"url":"#","service":"facebook"} /-->
<!-- wp:social-link {"url":"#","service":"instagram"} /-->
<!-- wp:social-link {"url":"#","service":"twitter"} /-->
</ul>
<!-- /wp:social-links -->
</div>
<!-- /wp:group -->
</div>
<!-- /wp:group -->
