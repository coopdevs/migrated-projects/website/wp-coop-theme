<?php
/**
 * Title: Section: Featured-boxes 2-columns background-color-main (text, button).
 * Slug: wpct/general-boxes-two-main-background
 * Categories: wpct-general
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"spacing":{"margin":{"top":"0px"}}},"backgroundColor":"main","layout":{"inherit":true}} -->
<div class="wp-block-group alignfull has-main-background-color has-background" style="margin-top:0px">
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide">
    <!-- wp:column -->
    <div class="wp-block-column">
        <!-- wp:group {"style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxxl)","right":"var(--wp--custom--spacing--sxxl)","bottom":"var(--wp--custom--spacing--sxxl)","left":"var(--wp--custom--spacing--sxxl)"}},"border":{"width":"0px","style":"none","radius":"0px"}},"backgroundColor":"base"} -->
        <div class="wp-block-group has-base-background-color has-background" style="border-radius:0px;border-style:none;border-width:0px;padding-top:var(--wp--custom--spacing--sxxl);padding-right:var(--wp--custom--spacing--sxxl);padding-bottom:var(--wp--custom--spacing--sxxl);padding-left:var(--wp--custom--spacing--sxxl)">
            <!-- wp:heading {"level":3,"fontSize":"large"} -->
            <h3 class="has-large-font-size" id="sample-heading-1"><?php echo esc_html__( 'Sample Heading', 'wpct' ); ?></h3>
            <!-- /wp:heading -->
            <!-- wp:paragraph -->
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing vestibulum. Fringilla nec accumsan eget, facilisis mi justo, luctus pellentesque vitae gravida non diam accumsan posuere, venenatis at mi turpis.</p>
            <!-- /wp:paragraph -->
            <!-- wp:buttons -->
            <div class="wp-block-buttons">
            <!-- wp:button {"style":{"border":{"radius":0}}} -->
            <div class="wp-block-button"><a class="wp-block-button__link no-border-radius"><?php echo esc_html__( 'Learn More', 'wpct' ); ?></a></div>
            <!-- /wp:button -->
            </div>
            <!-- /wp:buttons -->
        </div>
        <!-- /wp:group -->
    </div>
    <!-- /wp:column -->
    <!-- wp:column -->
    <div class="wp-block-column">
        <!-- wp:group {"style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxxl)","right":"var(--wp--custom--spacing--sxxl)","bottom":"var(--wp--custom--spacing--sxxl)","left":"var(--wp--custom--spacing--sxxl)"}},"border":{"width":"0px","style":"none","radius":"0px"}},"backgroundColor":"base"} -->
        <div class="wp-block-group has-base-background-color has-background" style="border-radius:0px;border-style:none;border-width:0px;padding-top:var(--wp--custom--spacing--sxxl);padding-right:var(--wp--custom--spacing--sxxl);padding-bottom:var(--wp--custom--spacing--sxxl);padding-left:var(--wp--custom--spacing--sxxl)">
            <!-- wp:heading {"level":3,"fontSize":"large"} -->
            <h3 class="has-large-font-size" id="sample-heading-2"><?php echo esc_html__( 'Sample Heading', 'wpct' ); ?></h3>
            <!-- /wp:heading -->
            <!-- wp:paragraph -->
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing vestibulum. Fringilla nec accumsan eget, facilisis mi justo, luctus pellentesque vitae gravida non diam accumsan posuere, venenatis at mi turpis.</p>
            <!-- /wp:paragraph -->
            <!-- wp:buttons -->
            <div class="wp-block-buttons">
            <!-- wp:button {"style":{"border":{"radius":0}}} -->
            <div class="wp-block-button"><a class="wp-block-button__link no-border-radius"><?php echo esc_html__( 'Learn More', 'wpct' ); ?></a></div>
            <!-- /wp:button -->
            </div>
            <!-- /wp:buttons -->
        </div>
        <!-- /wp:group -->
    </div>
    <!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
</div>
<!-- /wp:group -->
