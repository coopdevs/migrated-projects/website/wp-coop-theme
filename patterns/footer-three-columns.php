<?php
/**
 * Title: Footer: 3-columns (text, links, buttons).
 * Slug: wpct/footer-three-columns
 * Categories: wpct-footer
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxl)","bottom":"var(--wp--custom--spacing--sxl)"},"margin":{"top":"0px"}}},"layout":{"inherit":true},"fontSize":"small"} -->
<div class="wp-block-group alignfull has-small-font-size" style="margin-top:0px;padding-top:var(--wp--custom--spacing--sxl);padding-bottom:var(--wp--custom--spacing--sxl)">
<!-- wp:group {"align":"wide","layout":{"type":"flex","allowOrientation":false,"justifyContent":"space-between"}} -->
<div class="wp-block-group alignwide">
<!-- wp:paragraph -->
<p>© <?php echo gmdate( 'Y' ); ?> Your Cooperative</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph -->
<p><a href="#"><?php echo esc_html__( 'Privacy Policy', 'wpct' ); ?></a> · <a href="#"><?php echo esc_html__( 'Terms of Service', 'wpct' ); ?></a> · <a href="#"><?php echo esc_html__( 'Contact Us', 'wpct' ); ?></a></p>
<!-- /wp:paragraph -->
<!-- wp:social-links {"iconColor":"base","iconColorValue":"var(--wp--preset--color--base)","iconBackgroundColor":"main","iconBackgroundColorValue":"var(--wp--preset--color--main)","className":"is-style-default","style":{"spacing":{"blockGap":"var(--wp--custom--spacing--sxxs)"}}} -->
<ul class="wp-block-social-links has-icon-color has-icon-background-color is-style-default">
<!-- wp:social-link {"url":"#","service":"facebook"} /-->
<!-- wp:social-link {"url":"#","service":"instagram"} /-->
<!-- wp:social-link {"url":"#","service":"twitter"} /-->
</ul>
<!-- /wp:social-links -->
</div>
<!-- /wp:group -->
</div>
<!-- /wp:group -->
