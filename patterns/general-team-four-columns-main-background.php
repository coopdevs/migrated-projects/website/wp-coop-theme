<?php
/**
 * Title: Section: Team 4-columns background-color-main (image, text, link).
 * Slug: wpct/general-team-four-columns-main-background
 * Categories: wpct-general
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|main-contrast"}}},"spacing":{"margin":{"top":"0px"}}},"backgroundColor":"main","textColor":"main-contrast","layout":{"inherit":true}} -->
<div class="wp-block-group alignfull has-main-contrast-color has-main-background-color has-text-color has-background has-link-color" style="margin-top:0px">
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:heading {"textAlign":"center","fontSize":"x-large"} -->
<h2 class="has-text-align-center has-x-large-font-size" id="our-team"><?php echo esc_html__( 'Our Team', 'wpct' ); ?></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><?php echo esc_html__( 'The people who are ready to serve you.', 'wpct' ); ?></p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":60} -->
<div style="height:60px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide">
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:image {"id":3488,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="<?php echo esc_url( __( 'https://coopdevs.org/wp-content/uploads/2022/05/it-intercooperacio.jpg', 'wpct' ) ); ?>"  alt="<?php echo esc_attr__( 'Sample Image', 'wpct' ); ?>" class="wp-image-3488"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"textAlign":"center","level":3,"fontSize":"medium"} -->
<h3 class="has-text-align-center has-medium-font-size" id="member-name-1"><?php echo esc_html__( 'Member Name', 'wpct' ); ?></h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"elements":{"link":{"color":[]}},"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center has-link-color" style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur et adipiscing.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center" style="font-size:var(--wp--preset--font-size--small)"><a href="#"><?php echo esc_html__( 'View profile', 'wpct' ); ?></a>.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:image {"id":3488,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="<?php echo esc_url( __( 'https://coopdevs.org/wp-content/uploads/2022/05/it-intercooperacio.jpg', 'wpct' ) ); ?>"  alt="<?php echo esc_attr__( 'Sample Image', 'wpct' ); ?>" class="wp-image-3488"/></figure>
<!-- /wp:image -->
<!-- wp:heading {"textAlign":"center","level":3,"fontSize":"medium"} -->
<h3 class="has-text-align-center has-medium-font-size" id="member-name-2"><?php echo esc_html__( 'Member Name', 'wpct' ); ?></h3>
<!-- /wp:heading -->
<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center" style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur et adipiscing.</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center" style="font-size:var(--wp--preset--font-size--small)"><a href="#"><?php echo esc_html__( 'View profile', 'wpct' ); ?></a>.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:image {"id":3488,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="<?php echo esc_url( __( 'https://coopdevs.org/wp-content/uploads/2022/05/it-intercooperacio.jpg', 'wpct' ) ); ?>"  alt="<?php echo esc_attr__( 'Sample Image', 'wpct' ); ?>" class="wp-image-3488"/></figure>
<!-- /wp:image -->
<!-- wp:heading {"textAlign":"center","level":3,"fontSize":"medium"} -->
<h3 class="has-text-align-center has-medium-font-size" id="member-name-3"><?php echo esc_html__( 'Member Name', 'wpct' ); ?></h3>
<!-- /wp:heading -->
<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center" style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur et adipiscing.</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center" style="font-size:var(--wp--preset--font-size--small)"><a href="#"><?php echo esc_html__( 'View profile', 'wpct' ); ?></a>.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:image {"id":3488,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="<?php echo esc_url( __( 'https://coopdevs.org/wp-content/uploads/2022/05/it-intercooperacio.jpg', 'wpct' ) ); ?>"  alt="<?php echo esc_attr__( 'Sample Image', 'wpct' ); ?>" class="wp-image-3488"/></figure>
<!-- /wp:image -->
<!-- wp:heading {"textAlign":"center","level":3,"fontSize":"medium"} -->
<h3 class="has-text-align-center has-medium-font-size" id="member-name-4"><?php echo esc_html__( 'Member Name', 'wpct' ); ?></h3>
<!-- /wp:heading -->
<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center" style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur et adipiscing.</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p class="has-text-align-center" style="font-size:var(--wp--preset--font-size--small)"><a href="#"><?php echo esc_html__( 'View profile', 'wpct' ); ?></a>.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
</div>
<!-- /wp:group -->
