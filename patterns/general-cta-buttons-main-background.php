<?php
/**
 * Title: Section: Call-to-action-buttons background-color-main (text, buttons).
 * Slug: wpct/general-cta-buttons-main-background
 * Categories: wpct-general
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"spacing":{"margin":{"top":"0px"}}},"backgroundColor":"main","textColor":"main-contrast","layout":{"inherit":true}} -->
<div class="wp-block-group alignfull has-main-contrast-color has-main-background-color has-text-color has-background" style="margin-top:0px">
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide">
<!-- wp:column {"verticalAlignment":"center","width":"60%"} -->
<div class="wp-block-column is-vertically-aligned-center" style="flex-basis:60%">
<!-- wp:paragraph {"style":{"typography":{"lineHeight":"var(--wp--custom--line-height--medium)"}},"fontSize":"large"} -->
<p class="has-large-font-size" style="line-height:var(--wp--custom--line-height--medium)">Lorem ipsum dolor sit amet, consectetur adipiscing lectus. Vestibulum mi justo, luctus eu pellentesque vitae gravida non.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
<!-- wp:column {"verticalAlignment":"center","width":"40%"} -->
<div class="wp-block-column is-vertically-aligned-center" style="flex-basis:40%">
<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"right","orientation":"horizontal"},"style":{"spacing":{"blockGap":"var(--wp--custom--spacing--sxxs)"}}} -->
<div class="wp-block-buttons">
<!-- wp:button {"style":{"border":{"radius":0}},"className":"large is-style-fill-base"} -->
<div class="wp-block-button large is-style-fill-base"><a class="wp-block-button__link no-border-radius"><?php echo esc_html__( 'Get Started', 'wpct' ); ?> →</a></div>
<!-- /wp:button -->
<!-- wp:button {"style":{"border":{"radius":0}},"className":"large is-style-outline-main-contrast"} -->
<div class="wp-block-button large is-style-outline-main-contrast"><a class="wp-block-button__link no-border-radius"><?php echo esc_html__( 'Learn More', 'wpct' ); ?></a></div>
<!-- /wp:button -->
</div>
<!-- /wp:buttons -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
</div>
<!-- /wp:group -->
