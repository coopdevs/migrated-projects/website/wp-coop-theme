<?php
/**
 * Title: Footer: Mega (text, button, links).
 * Slug: wpct/footer-mega
 * Categories: wpct-footer
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"},"spacing":{"margin":{"top":"0px"}}},"layout":{"inherit":true}} -->
<div class="wp-block-group alignfull has-small-font-size" style="font-size:var(--wp--preset--font-size--small);margin-top:0px">
<!-- wp:columns {"align":"wide","style":{"elements":{"link":{"color":[]}},"spacing":{"padding":{"top":"var(--wp--custom--spacing--sspacer)","bottom":"var(--wp--custom--spacing--sspacer)"},"blockGap":"var(--wp--custom--spacing--sspacer)"}}} -->
<div class="wp-block-columns alignwide has-link-color" style="padding-top:var(--wp--custom--spacing--sspacer);padding-bottom:var(--wp--custom--spacing--sspacer)">
<!-- wp:column {"width":"50%"} -->
<div class="wp-block-column" style="flex-basis:50%">
<!-- wp:heading {"level":4} -->
<h4 id="our-company"><?php echo esc_html__( 'Our Cooperative', 'wpct' ); ?></h4>
<!-- /wp:heading -->
<!-- wp:paragraph -->
<p>Lorem ipsum dolor sit amet, consectetur adipiscing lectus. Vestibulum mi justo, luctus eu pellentesque vitae gravida non.</p>
<!-- /wp:paragraph -->
<!-- wp:buttons -->
<div class="wp-block-buttons">
<!-- wp:button {"style":{"border":{"radius":0}},"className":"is-style-fill"} -->
<div class="wp-block-button is-style-fill">
<a class="wp-block-button__link no-border-radius" href="#"><?php echo esc_html__( 'Learn More', 'wpct' ); ?></a>
</div>
<!-- /wp:button -->
</div>
<!-- /wp:buttons -->
</div>
<!-- /wp:column -->
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:columns -->
<div class="wp-block-columns">
<!-- wp:column {"width":"33%"} -->
<div class="wp-block-column" style="flex-basis:33%">
<!-- wp:heading {"level":4} -->
<h4 id="about-us"><?php echo esc_html__( 'About Us', 'wpct' ); ?></h4>
<!-- /wp:heading -->
<!-- wp:list {"fontSize":"small"} -->
<ul class="has-small-font-size">
<li><a href="#"><?php echo esc_html__( 'Start Here', 'wpct' ); ?></a></li>
<li><a href="#"><?php echo esc_html__( 'Our Mission', 'wpct' ); ?></a></li>
<li><a href="#"><?php echo esc_html__( 'Brand Guide', 'wpct' ); ?></a></li>
<li><a href="#"><?php echo esc_html__( 'Newsletter', 'wpct' ); ?></a></li>
<li><a href="#"><?php echo esc_html__( 'Accessibility', 'wpct' ); ?></a></li>
</ul>
<!-- /wp:list -->
</div>
<!-- /wp:column -->
<!-- wp:column {"width":"33%"} -->
<div class="wp-block-column" style="flex-basis:33%">
<!-- wp:heading {"level":4} -->
<h4 id="services"><?php echo esc_html__( 'Services', 'wpct' ); ?></h4>
<!-- /wp:heading -->
<!-- wp:list {"fontSize":"small"} -->
<ul class="has-small-font-size">
<li><a href="#"><?php echo esc_html__( 'Web Design', 'wpct' ); ?></a></li>
<li><a href="#"><?php echo esc_html__( 'Development', 'wpct' ); ?></a></li>
<li><a href="#"><?php echo esc_html__( 'Copywriting', 'wpct' ); ?></a></li>
<li><a href="#"><?php echo esc_html__( 'Marketing', 'wpct' ); ?></a></li>
<li><a href="#"><?php echo esc_html__( 'Social Media', 'wpct' ); ?></a></li>
</ul>
<!-- /wp:list -->
</div>
<!-- /wp:column -->
<!-- wp:column {"width":"33%"} -->
<div class="wp-block-column" style="flex-basis:33%">
<!-- wp:heading {"level":4} -->
<h4 id="connect"><?php echo esc_html__( 'Connect', 'wpct' ); ?></h4>
<!-- /wp:heading -->
<!-- wp:list {"fontSize":"small"} -->
<ul class="has-small-font-size">
<li><a href="#">Facebook</a></li>
<li><a href="#">Instagram</a></li>
<li><a href="#">Twitter</a></li>
<li><a href="#">LinkedIn</a></li>
<li><a href="#">Dribbble</a></li>
</ul>
<!-- /wp:list -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
</div>
<!-- /wp:group -->
