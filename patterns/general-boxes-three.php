<?php
/**
 * Title: Section: Featured-boxes 3-columns (text, button).
 * Slug: wpct/general-boxes-three
 * Categories: wpct-general
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"spacing":{"margin":{"top":"0px"}}},"textColor":"main-contrast","layout":{"inherit":true}} -->
<div class="wp-block-group alignfull has-main-contrast-color has-text-color" style="margin-top:0px">
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide">
<!-- wp:column -->
<div class="wp-block-column">
    <!-- wp:group {"style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxl)","right":"var(--wp--custom--spacing--sxl)","bottom":"var(--wp--custom--spacing--sxl)","left":"var(--wp--custom--spacing--sxl)"}},"border":{"width":"0px","style":"none","radius":"0px"}},"backgroundColor":"main"} -->
    <div class="wp-block-group has-main-background-color has-background" style="border-radius:0px;border-style:none;border-width:0px;padding-top:var(--wp--custom--spacing--sxl);padding-right:var(--wp--custom--spacing--sxl);padding-bottom:var(--wp--custom--spacing--sxl);padding-left:var(--wp--custom--spacing--sxl)">
        <!-- wp:heading {"level":3} -->
        <h3 id="sample-heading-1"><?php echo esc_html__( 'Sample Heading', 'wpct' ); ?></h3>
        <!-- /wp:heading -->
        <!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
        <p style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur adipiscing vestibulum. Fringilla nec accumsan eget, facilisis mi justo, luctus pellentesque.</p>
        <!-- /wp:paragraph -->
        <!-- wp:buttons -->
        <div class="wp-block-buttons">
        <!-- wp:button {"style":{"border":{"radius":0}},"className":"is-style-fill-base"} -->
        <div class="wp-block-button is-style-fill-base"><a class="wp-block-button__link no-border-radius"><?php echo esc_html__( 'Learn More', 'wpct' ); ?></a></div>
        <!-- /wp:button -->
        </div>
        <!-- /wp:buttons -->
    </div>
    <!-- /wp:group -->
</div>
<!-- /wp:column -->
<!-- wp:column -->
<div class="wp-block-column">
    <!-- wp:group {"style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxl)","right":"var(--wp--custom--spacing--sxl)","bottom":"var(--wp--custom--spacing--sxl)","left":"var(--wp--custom--spacing--sxl)"}},"border":{"width":"0px","style":"none","radius":"0px"}},"backgroundColor":"main"} -->
    <div class="wp-block-group has-main-background-color has-background" style="border-radius:0px;border-style:none;border-width:0px;padding-top:var(--wp--custom--spacing--sxl);padding-right:var(--wp--custom--spacing--sxl);padding-bottom:var(--wp--custom--spacing--sxl);padding-left:var(--wp--custom--spacing--sxl)">
        <!-- wp:heading {"level":3} -->
        <h3 id="sample-heading-1"><?php echo esc_html__( 'Sample Heading', 'wpct' ); ?></h3>
        <!-- /wp:heading -->
        <!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
        <p style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur adipiscing vestibulum. Fringilla nec accumsan eget, facilisis mi justo, luctus pellentesque.</p>
        <!-- /wp:paragraph -->
        <!-- wp:buttons -->
        <div class="wp-block-buttons">
        <!-- wp:button {"style":{"border":{"radius":0}},"className":"is-style-fill-base"} -->
        <div class="wp-block-button is-style-fill-base"><a class="wp-block-button__link no-border-radius"><?php echo esc_html__( 'Learn More', 'wpct' ); ?></a></div>
        <!-- /wp:button -->
        </div>
        <!-- /wp:buttons -->
    </div>
    <!-- /wp:group -->
</div>
<!-- /wp:column -->
<!-- wp:column -->
<div class="wp-block-column">
    <!-- wp:group {"style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxl)","right":"var(--wp--custom--spacing--sxl)","bottom":"var(--wp--custom--spacing--sxl)","left":"var(--wp--custom--spacing--sxl)"}},"border":{"width":"0px","style":"none","radius":"0px"}},"backgroundColor":"main"} -->
    <div class="wp-block-group has-main-background-color has-background" style="border-radius:0px;border-style:none;border-width:0px;padding-top:var(--wp--custom--spacing--sxl);padding-right:var(--wp--custom--spacing--sxl);padding-bottom:var(--wp--custom--spacing--sxl);padding-left:var(--wp--custom--spacing--sxl)">
        <!-- wp:heading {"level":3} -->
        <h3 id="sample-heading-1"><?php echo esc_html__( 'Sample Heading', 'wpct' ); ?></h3>
        <!-- /wp:heading -->
        <!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
        <p style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur adipiscing vestibulum. Fringilla nec accumsan eget, facilisis mi justo, luctus pellentesque.</p>
        <!-- /wp:paragraph -->
        <!-- wp:buttons -->
        <div class="wp-block-buttons">
        <!-- wp:button {"style":{"border":{"radius":0}},"className":"is-style-fill-base"} -->
        <div class="wp-block-button is-style-fill-base"><a class="wp-block-button__link no-border-radius"><?php echo esc_html__( 'Learn More', 'wpct' ); ?></a></div>
        <!-- /wp:button -->
        </div>
        <!-- /wp:buttons -->
    </div>
    <!-- /wp:group -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
</div>
<!-- /wp:group -->
