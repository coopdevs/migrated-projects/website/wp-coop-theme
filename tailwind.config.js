const _ = require("lodash");
const tailpress = require("@jeffreyvr/tailwindcss-tailpress");

module.exports = {
  content: [
    './*/*.html',
    './**/*.html',
    './assets/css/*.css',
    './assets/js/*.js',
    './safelist.txt'
  ],
  theme: {
    extend: {},
  },
  plugins: [
    tailpress.tailwind
  ]
}